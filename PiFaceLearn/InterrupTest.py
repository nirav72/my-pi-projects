import pifacedigitalio
pifacedigitalio.init()
pfd = pifacedigitalio.PiFaceDigital()
def toggle_led0(event):
  pfd.leds[0].toggle()
  listener = pifacedigitalio.InputEventListener()
  listener.register(0, pifacedigitalio.IODIR_ON, toggle_led0)
  listener.activate()
